import React from 'react';
import PropTypes from 'prop-types';

/**
 * ErrorBoundary
 * @description [description]
 * @example
  <div id="ErrorBoundary"></div>
  <script>
	ReactDOM.render(React.createElement(Components.ErrorBoundary, {
	}), document.getElementById("ErrorBoundary"));
  </script>
 */
class ErrorBoundary extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'error-boundary';

		this.state = {error: null, info: null};
	}

	componentDidCatch(error, info) {
		this.setState({error, info});
	}

	render() {
		const {error, info} = this.state;
		const {altContent, children} = this.props;

		if (error) {
			console.warn('React component failed to render', info);

			return (
				<details style={{whiteSpace: 'pre-wrap'}}>
					{this.state.error && this.state.error.toString()}
					<br />
					{this.state.errorInfo.componentStack}
				</details>
			);

			// TODO: check this

			if (altContent) {
				return altContent;
			}

			if (info) {
				return (
					<div>
						<h1>Something went wrong</h1>
					</div>
				);
			}

			return <div>Your react component failed to render</div>;
		}

		/*

		 */

		return children;
	}
}

ErrorBoundary.defaultProps = {
	altContent: null,
	children: ''
};

ErrorBoundary.propTypes = {
	altContent: PropTypes.string,
	children: PropTypes.node
};

export default ErrorBoundary;
